package com.alterra.springbootgraphql.resolver;

import com.alterra.springbootgraphql.domain.dto.AuthorDto;
import com.alterra.springbootgraphql.domain.dto.PostDto;
import com.alterra.springbootgraphql.service.AuthorService;
import com.coxautodev.graphql.tools.GraphQLResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PostResolver implements GraphQLResolver<PostDto> {

    @Autowired
    private AuthorService authorService;

    public AuthorDto getAuthor(PostDto request) {
        log.info("Request: {}", request);
        return authorService.getAuthorById(request.getAuthorId());
    }

}
