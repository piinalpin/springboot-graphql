package com.alterra.springbootgraphql.resolver;

import com.alterra.springbootgraphql.domain.dto.AuthorDto;
import com.alterra.springbootgraphql.domain.dto.PostDto;
import com.alterra.springbootgraphql.service.PostService;
import com.coxautodev.graphql.tools.GraphQLResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AuthorResolver implements GraphQLResolver<AuthorDto> {

    @Autowired
    private PostService postService;

    public List<PostDto> getPosts(AuthorDto authorDto) {
        log.info("Get post by author id. Request: {}", authorDto);
        return postService.getAllPostByAuthorId(authorDto.getId());
    }

}
