package com.alterra.springbootgraphql.resolver.mutation;

import com.alterra.springbootgraphql.domain.dto.AuthorDto;
import com.alterra.springbootgraphql.service.AuthorService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthorMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private AuthorService authorService;

    public AuthorDto addAuthor(AuthorDto request) {
        return authorService.addAuthor(request);
    }

    public AuthorDto updateAuthor(AuthorDto request, Long id) {
        log.info("Execute update author mutation");
        return authorService.updateAuthor(request, id);
    }

}
