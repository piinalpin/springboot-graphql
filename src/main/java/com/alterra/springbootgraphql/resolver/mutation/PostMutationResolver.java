package com.alterra.springbootgraphql.resolver.mutation;

import com.alterra.springbootgraphql.domain.dto.PostDto;
import com.alterra.springbootgraphql.service.PostService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PostMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private PostService postService;

    public PostDto writePost(PostDto request) {
        log.info("Post Mutation resolver with input form {}", request);
        return postService.writePost(request);
    }


}
