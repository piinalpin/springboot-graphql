package com.alterra.springbootgraphql.resolver.query;

import com.alterra.springbootgraphql.domain.dto.AuthorDto;
import com.alterra.springbootgraphql.service.AuthorService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private AuthorService authorService;

    public List<AuthorDto> getAllAuthor() {
        return authorService.getAllAuthor();
    }

}
