package com.alterra.springbootgraphql.resolver.query;

import com.alterra.springbootgraphql.domain.dto.PostDto;
import com.alterra.springbootgraphql.service.PostService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private PostService postService;

    public List<PostDto> getAllPost() {
        return postService.getAllPost();
    }

    public List<PostDto> recentPost(Integer limit, Integer offset) {
        return postService.getRecentPost(limit, offset);
    }

}
