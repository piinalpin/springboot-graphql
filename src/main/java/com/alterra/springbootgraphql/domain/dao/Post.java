package com.alterra.springbootgraphql.domain.dao;

import com.alterra.springbootgraphql.domain.common.BaseDao;
import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "POST")
@Entity
public class Post extends BaseDao {

    private static final long serialVersionUID = 4405899046952865002L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Author author;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "category")
    private String category;

}
