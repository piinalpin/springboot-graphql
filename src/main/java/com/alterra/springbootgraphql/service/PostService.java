package com.alterra.springbootgraphql.service;

import com.alterra.springbootgraphql.domain.dao.Author;
import com.alterra.springbootgraphql.domain.dao.Post;
import com.alterra.springbootgraphql.domain.dto.PostDto;
import com.alterra.springbootgraphql.repository.AuthorRepository;
import com.alterra.springbootgraphql.repository.PostRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PostService {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private PostRepository postRepository;

    public PostDto writePost(PostDto request) {
        log.info("Start executing write new post.");
        log.debug("Executing write post with request: {}", request);

        try {
            Optional<Author> author = authorRepository.findById(request.getAuthorId());
            if (author.isEmpty()) throw new IllegalArgumentException("Author not found");

            Post post = mapper.map(request, Post.class);
            post.setAuthor(author.get());
            post = postRepository.save(post);
            return mapper.map(post, PostDto.class);
        } catch (Exception e) {
            log.error("Happened error when write post. Error: {}", e.getMessage());
            log.trace("Get error when write post. ", e);
            throw e;
        }
    }

    public List<PostDto> getAllPost() {
        log.info("Start executing get all post.");

        try {
            List<Post> posts = postRepository.findAll();
            return mappingPostDtoList(posts);
        } catch (Exception e) {
            log.error("Happened error when get all post. Error: {}", e.getMessage());
            log.trace("Get error when get all post. ", e);
            throw e;
        }
    }

    public List<PostDto> getAllPostByAuthorId(Long authorId) {
        log.info("Start executing get all post by author id.");

        try {
            Optional<Author> author = authorRepository.findById(authorId);
            if (author.isEmpty()) throw new IllegalArgumentException("Author not found");

            List<Post> posts = postRepository.findAllByAuthor(author.get());
            return mappingPostDtoList(posts);
        } catch (Exception e) {
            log.error("Happened error when get all post by author id. Error: {}", e.getMessage());
            log.trace("Get error when get all post by author id. ", e);
            throw e;
        }
    }

    public List<PostDto> getRecentPost(Integer limit, Integer offset) {
        log.info("Start executing get recent post.");

        try {
            List<Post> posts = postRepository.getRecentPosts(limit, offset);
            return mappingPostDtoList(posts);
        } catch (Exception e) {
            log.error("Happened error when get recent post. Error: {}", e.getMessage());
            log.trace("Get error when get recent post. ", e);
            throw e;
        }
    }

    private List<PostDto> mappingPostDtoList(List<Post> posts) {
        List<PostDto> postDtoList = new ArrayList<>();

        for (Post post : posts) {
            postDtoList.add(mapper.map(post, PostDto.class));
        }

        log.info("Returning post list. Size: {}", postDtoList.size());
        return postDtoList;
    }

}
