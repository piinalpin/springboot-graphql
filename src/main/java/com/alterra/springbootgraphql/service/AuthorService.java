package com.alterra.springbootgraphql.service;

import com.alterra.springbootgraphql.domain.dao.Author;
import com.alterra.springbootgraphql.domain.dto.AuthorDto;
import com.alterra.springbootgraphql.repository.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private ModelMapper mapper;

    public AuthorDto getAuthorById(Long id) {
        log.info("Start executing get author by id.");
        log.debug("Executing get author by ID: {}", id);

        try {
            Optional<Author> author = authorRepository.findById(id);
            if (author.isEmpty()) throw new IllegalArgumentException("Author not found");

            return mapper.map(author.get(), AuthorDto.class);
        } catch (Exception e) {
            log.error("Happened error when get author by id. Error: {}", e.getMessage());
            log.trace("Get error when get author by id. ", e);
            throw e;
        }
    }

    public AuthorDto addAuthor(AuthorDto request) {
        log.info("Start executing add author.");
        log.debug("Executing add author with request: {}", request);

        try {
            Author author = mapper.map(request, Author.class);
            authorRepository.save(author);

            return mapper.map(author, AuthorDto.class);
        } catch (Exception e) {
            log.error("Happened error when add author. Error: {}", e.getMessage());
            log.trace("Get error when save new author. ", e);
            throw e;
        }
    }

    public AuthorDto updateAuthor(AuthorDto request, Long id) {
        log.info("Start executing update author.");
        log.debug("Executing update author with request: {}", request);

        try {
            Optional<Author> author = authorRepository.findById(id);
            if (author.isEmpty()) throw new IllegalArgumentException("Author not found");

            mapper.map(request, author.get());
            author.get().setId(id);
            authorRepository.save(author.get());

            return mapper.map(author.get(), AuthorDto.class);
        } catch (Exception e) {
            log.error("Happened error when update author. Error: {}", e.getMessage());
            log.trace("Get error when update author. ", e);
            throw e;
        }
    }

    public List<AuthorDto> getAllAuthor() {
        log.info("Start executing get all author");

        try {
            List<Author> authors = authorRepository.findAll();
            List<AuthorDto> authorDtoList = new ArrayList<>();
            for (Author author : authors) {
                authorDtoList.add(mapper.map(author, AuthorDto.class));
            }

            log.info("Returning author list. Size: {}", authorDtoList.size());
            return authorDtoList;
        } catch (Exception e) {
            log.error("Happened error when get all author. Error: {}", e.getMessage());
            log.trace("Get error when get all author. ", e);
            throw e;
        }
    }

}
