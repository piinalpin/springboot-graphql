package com.alterra.springbootgraphql.component;

import com.alterra.springbootgraphql.domain.dao.Author;
import com.alterra.springbootgraphql.repository.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("Add author data when application staring for testing");
        List<Author> authors = List.of(
                Author.builder()
                        .name("Maverick")
                        .thumbnail("maverick.jpg")
                        .build(),
                Author.builder()
                        .name("Alvin")
                        .thumbnail("alvin.jpg")
                        .build()
        );
        authorRepository.saveAll(authors);
        log.info("Done add author data for testing");
    }

}
