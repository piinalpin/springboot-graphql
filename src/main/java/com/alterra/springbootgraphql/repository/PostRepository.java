package com.alterra.springbootgraphql.repository;

import com.alterra.springbootgraphql.domain.dao.Author;
import com.alterra.springbootgraphql.domain.dao.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query(value = "SELECT * FROM POST ORDER BY created_at DESC LIMIT :limit OFFSET :offset", nativeQuery = true)
    List<Post> getRecentPosts(@Param("limit") Integer limit, @Param("offset") Integer offset);

    List<Post> findAllByAuthor(Author author);

}
